#!/bin/ash
# Copyright (C) 2006 - present, Stephan Enderlein<stephan@freifunk-dresden.de>
# GNU General Public License Version 3

delay=10	# seconds
dhcp_trigger_refresh_count=6	# refresh dhcp after x loops

lte_info_dir="/var/lib/ddmesh"
lte_info="$lte_info_dir/lte_info"

mkdir -p $lte_info_dir
wwan_device="$(uci -q get network.wwan.device)"
syslog="$(uci -q get ddmesh.network.wwan_syslog)"

# check if hw is present
if ! uqmi -s -d $wwan_device --get-capabilities 2>&1 >/dev/null; then
	logger -s -t "lte-modem" "no LTE modem present -> exit monitor"
	exit 1
fi
logger -s -t "lte-modem" "LTE modem present -> start monitoring"

dhcp_refresh_count=0
while true;
do

	# kill is needed to avoid deadlocks
	killall uqmi 2>/dev/null

	signal="$(uqmi -s -d $wwan_device --get-signal-info)"
	status="$(uqmi -s -d $wwan_device --get-data-status)"
	pin_state="$(uqmi -s -d $wwan_device --uim-get-sim-state)"
	system_info="$(uqmi -d $wwan_device --get-system-info)"
	service="$(uqmi -s -d $wwan_device --get-serving-system)"
	phyinfo="$(uqmi -s -d $wwan_device --get-lte-cphy-ca-info)"

	eval $(echo "$system_info" | jsonfilter -e m_mcc='@.lte.mcc' -e m_mnc='@.lte.mnc')
	# https://mcc-mnc.net/
	ocode="$m_mcc:$m_mnc"
	case "${ocode}" in
		"262:01"|"262:06"|"262:78") operator="Telekom Deutschland GmbH" ;;
		"262:02"|"262:04"|"262:09") operator="Vodafone D2 GmbH" ;;
		"262:03"|"262:05"|"262:07"|"262:08"|"262:11"|"262:17"|"262:77") operator="Telefonica Germany GmbH" ;;
		"262:23") operator="Drillisch Online AG" ;;
		*) operator="$ocode" ;;
	esac


cat<<EOM > $lte_info.tmp
	{
	"signal": $signal,
	"status": $status,
	"service": $service,
	"pin_state" : $pin_state,
	"system_info" : $system_info,
	"phy_info" : $phyinfo,
	"other" : { "operator":"$operator" }
	}
EOM
	mv $lte_info.tmp $lte_info
	json="$(cat $lte_info)"
	[ "$syslog" = "1" ] && logger -t "LTE" "$json"

	# read lte status
	eval $(cat $lte_info | jsonfilter -e m_type='@.signal.type' \
		-e m_rssi='@.signal.rssi' \
		-e m_rsrq='@.signal.rsrq' \
		-e m_rsrp='@.signal.rsrp' \
		-e m_snr='@.signal.snr' \
		-e m_conn='@.status' \
		-e m_mcc='@.system_info.lte.mcc' \
		-e m_mnc='@.system_info.lte.mnc' \
		-e registration='@.service.registration')

	if [ "$m_conn" = "connected" -a "$registration" = "registered" ]; then
		case "$m_type" in
			gsm) /usr/lib/ddmesh/ddmesh-led.sh wwan 2g ;;
			wcdma) /usr/lib/ddmesh/ddmesh-led.sh wwan 3g ;;
			lte) /usr/lib/ddmesh/ddmesh-led.sh wwan 4g ;;
			*) /usr/lib/ddmesh/ddmesh-led.sh wwan off ;;
		esac
	else
		/usr/lib/ddmesh/ddmesh-led.sh wwan off
	fi

	dhcp_refresh_count=$((dhcp_refresh_count + 1))
	if [ $dhcp_refresh_count -gt $dhcp_trigger_refresh_count ];then
		kill -USR1 $(cat /var/run/udhcpc-wwan0.pid)
		dhcp_refresh_count=0
	fi

	sleep $delay
done
